import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Home from './pages/Home';
import Dotnet from './pages/Dotnet';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import JavaScript from './pages/JavaScript';
import OpenSource from './pages/OpenSource';
import Math from './pages/Math';
import Sql from './pages/Sql';
import DockerK8s from './pages/DockerK8s';
import Blog from './components/Blog';
import { MathJaxContext } from 'better-react-mathjax';

const config = {
  tex: {
    inlineMath: [["$", "$"]]
  }
}
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <MathJaxContext config={config}>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/dotnet" element={<Dotnet />} />
        <Route path="/sql" element={<Sql />} />
        <Route path="/dockerk8s" element={<DockerK8s />} />
        <Route path="/math" element={<Math />} />
        <Route path="/math/:name" element={<Blog />} />
        <Route path="/javascript" element={<JavaScript />} />
        <Route path="/opensource" element={<OpenSource />} />
      </Routes>
    </BrowserRouter>
  </MathJaxContext>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
