import React, { useState, useEffect } from 'react';
import Typography from '@mui/material/Typography';
import { createTheme } from '@mui/material/styles';
import SkillTemplate from './SkillTemplate';
import { useParams } from "react-router-dom";
import { MathJax } from 'better-react-mathjax';
import fetchBlog from '../graphql/Queries';

const theme = createTheme({
  palette: {
    primary: {
      main: '#265F36',
      light: '#E197ED'
    },
    secondary: {
      main: '#95A3A4'
    }
  }
});

const mapInline = (contents, parentId) => {
  return <MathJax key={parentId}>
    {contents.map(y => {
      if (y.Type.toLowerCase() === "inlinemath") {
        return (`$${y.Text}$`)
      }
      else {
        return (y.Text)
      }
    })}
  </MathJax>
}

const getMathJax = (raw, id) => (
  <MathJax key={id}>
    {raw}
  </MathJax>
)

export default function Blog() {
  const [blog, setBlog] = useState([]);
  const name = useParams().name;
  useEffect(() => {
    fetchBlog(name, setBlog)
  }, [])

  return (
    <SkillTemplate theme={theme} skill='Blogs'>
      <div style={{
        padding: '0 20vw'
      }}>
        <Typography variant='p'>
          {blog.map((x, id) => {
            let div = <div></div>;
            if (x.Type === 'h1') {
              div = 
                <Typography key={id} variant='h1' align='center'>
                  {mapInline(x.Contents, id)}
                </Typography>
            }
            else if (x.Type[0] === 'h' && x.Type.length === 2) {
              div = 
                <Typography key={id} variant={x.Type}>
                  {mapInline(x.Contents, id)}
                </Typography>
            }
            else if (x.Type === 'displaymath') {
              div = getMathJax(String.raw`\[${x.Content}\]`, id)
            }
            else if (x.Type === 'align') {
              div = getMathJax(String.raw`\begin{align}${x.Content}\end{align}`, id)
            }
            else if (x.Type === 'break') {
              div = <br/>
            }
            else {
              div = 
                <Typography key={id} variant="p" align="justify">
                  {mapInline(x.Contents, id)}
                </Typography>
            }
            return div;
          })}
        </Typography>
      </div>
    </SkillTemplate>
  );
}
