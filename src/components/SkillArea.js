import * as React from 'react';
import SkillCard from "./SkillCard";

import docker from "../images/docker.jpg";
import math from "../images/math.png";
import dotnet from "../images/dotnet.jpg";
import sql from "../images/sql.jpg";
import js from "../images/js.jpg";
import openSource from "../images/openSource.png";
import csharpLogo from "../images/csharpLogo.svg";
import sqlLogo from "../images/sqlLogo.png";
import dockerLogo from "../images/dockerLogo.png";
import jsLogo from "../images/jsLogo.png";
import mathLogo from "../images/mathLogo.png";
import openSourceLogo from "../images/openSourceLogo.png";

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

export default function SkillArea() {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={0}>
                <Grid xs={4}>
                    <SkillCard link='/dotnet' skill='.NET' background={dotnet} logo={csharpLogo}/>
                </Grid>
                <Grid xs={4}>
                    <SkillCard link='/sql' skill='SQL' background={sql} logo={sqlLogo}/>
                </Grid>
                <Grid xs={4}>
                    <SkillCard link='/dockerk8s' skill='Docker + k8s' background={docker} logo={dockerLogo}/>
                </Grid>
                <Grid xs={4}>
                    <SkillCard link='/math' skill='Math' background={math} logo={mathLogo}/>
                </Grid>
                <Grid xs={4}>
                    <SkillCard link='/javascript' skill='JavaScript' background={js} logo={jsLogo}/>
                </Grid>
                <Grid xs={4}>
                    <SkillCard link='/opensource' skill='Open Source' background={openSource} logo={openSourceLogo}/>
                </Grid>
            </Grid>
        </Box>
    );
}