import * as React from 'react';
import Navbar from '../components/Navbar';

export default function JavaScript() {
    return (
        <Navbar skill='JavaScript'/>
    );
}