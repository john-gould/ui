import * as React from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';

export default function DotnetProfile(props) {
  const bull = (
    <Box
      component="span"
      sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
    >
      •
    </Box>
  );
  return (
    <Box sx={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'flex-start',
      maxWidth: `${props.vw}vw`,
      padding: '0 40px'
    }}>
      <Typography variant='h4' align='center'>
        {props.title}
      </Typography>
      {[].concat(...props.subTexts.map(item => <Typography variant='p' align='center'>
        {item}
      </Typography>)
        .map(n => [n, bull])).slice(0,-1)}
      <Box sx={{paddingTop: '10px'}}>
        {props.icon}
      </Box>
    </Box>
  );
}