export default function fetchBlog(name, setBlog) {
  fetch(`http://localhost:5071/graphql`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "query": `query{blog(name: "${name}") { json  }}`
    })
  })
  .then((response) => response.json())
  .then((data) => {
    if (data) {
      setBlog(JSON.parse(data.data.blog.json));
    }
  });
}