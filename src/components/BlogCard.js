import React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { IconButton, Link } from '@mui/material';
import { OpenInFull } from '@mui/icons-material';

export default function BlogCard(props) {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardHeader
        action={<Link href='math/dampedHarmonicMotion'><IconButton><OpenInFull /></IconButton></Link>}
        title={props.title}
        subheader={props.date}
      />
      <CardMedia
        component="img"
        height="194"
        image={props.image}
        alt={props.alt}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {props.description}
        </Typography>
      </CardContent>
    </Card>
  );
}
