import React from 'react';
import { createTheme } from '@mui/material/styles';
import SkillTemplate from '../components/SkillTemplate';
import BlogCard from '../components/BlogCard';
import dampedOscillations from "../images/dampedOscillations.jpeg";

const theme = createTheme({
  palette: {
    primary: {
      main: '#265F36',
      light: '#E197ED'
    },
    secondary: {
      main: '#95A3A4'
    }
  }
});

export default function Math() {
  return (
    <SkillTemplate theme={theme} skill='Math'>
      <BlogCard
        title='Damped Harmonic Motion'
        date='September 14, 2022'
        description='The damped harmonic differential equation gives rise to different
motions depending on the size of the damping term. This blog
explores the mathematics of each case.'
        image={dampedOscillations}
        alt='Damped Oscillations' />
    </SkillTemplate>
  );
}