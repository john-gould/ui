import * as React from 'react';
import { createTheme } from '@mui/material/styles';
import DotnetCrafts from '../components/DotnetCrafts';
import DotnetProfiles from '../components/DotnetProfiles';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import DataObjectIcon from '@mui/icons-material/DataObject';
import ScienceIcon from '@mui/icons-material/Science';
import WorkspacesIcon from '@mui/icons-material/Workspaces';
import UpgradeIcon from '@mui/icons-material/Upgrade';
import SkillTemplate from '../components/SkillTemplate';

const theme = createTheme({
  palette: {
    primary: {
      main: '#6A1577',
      light: '#E197ED'
    },
    secondary: {
      main: '#068D9D'
    }
  }
});

const solid = [
  {
    title: 'Single Responsibility',
    code: [
      {
        title: 'Poor Code',
        code: `public class StatementGenerator
{
    public Statement CreateStatement(int personId, DateTime from, DateTime to)
    {
        // this method has to get transactions
        var transactions = _repository.Query<Transaction>(t => t.Person.Id == personId)
                                      .Where(t.Timestamp < to)
                                      .Where(t.Timestamp > from)
                                      .ToList();

        // AND format them
        var formattedLines = new List<string>();
        foreach (var t in transactions)
        {
            formattedLines.Add($"{t.Timestamp} | {t.Amount} | {t.Description}");
        }
        return new Statement
        {
            Person = _repository.Get<Person>(personId).Name, // CreateStatement method should not
                                                             // have to find name 
            Transactions = formattedLines
        };
    }
}`
      },
      {
        title: 'Good Code',
        code: `// Getting transactions pulled out to its own class
public class TransactionRepository
{
    public IEnumerable<Transaction> GetTransactionsForPerson(int personId, DateTime from, DateTime to) =>
        _repository.Query<Transaction>(t => t.Person.Id == personId)
                   .Where(t.Timestamp < to)
                   .Where(t.Timestamp > from)
                   .ToList();
}
public class StatementGenerator
{
    // sole job is to format the transaction lines
    public Statement CreateStatement(string name, IEnumerable<Transaction> transactions)
    {
        var formattedLines = new List<string>();
        foreach (var t in transactions)
        {
            formattedLines.Add($"{t.Timestamp} | {t.Amount} | {t.Description}");
        }
        return new Statement
        {
            Person = name, // name passed into method
            Transactions = formattedLines
        };
    }
}`
      }
    ]
  },
  {
    title: 'Open/Closed',
    code: [
      {
        title: 'Poor Code',
        code: `public class StatementManager
{
    private readonly ISmsManager _smsManager:
    private readonly IEmailManager _emailManager:

    public StatementManager(ISmsManager smsManager, IEmailManager emailManager)
    {
        _smsManager = smsManager;
        _emailManager = emailManager;
    }

    public SendResponse SendStatement(Statement statement, Person person, SendChannel channel)
    {
        try
        {
            // If more channels are made, this method will have to be modified
            // to cater for them
            if (channel == SendChannel.Sms)
            {
                _smsManager.Send(statement, person);
            }
            else if (channel == SendChannel.Email)
            {
                _emailManager.Send(statement, person);
            }
        }
        catch (Exception ex)
        {
            return new()
            {
                Success = false,
                Message = ex.Message
            };
        }

        return new()
        {
            Success = true,
            Sent = DateTime.Now
        };
    }
}

public enum SendChannel
{
    Sms,
    Email
}`
      },
      {
        title: 'Good Code',
        code: `public class StatementManager
{
    private readonly ICommsManager _commsManager:

    public StatementManager(ICommsManager commsManager)
    {
        _commsManager = commsManager;
    }

    public SendResponse SendStatement(Statement statement, Person person)
    {
        try
        {
            // regardless of the concrete manager used
            // this method will not have to be modified
            _commsManager.SendStatement(statement, person);
        }
        catch (Exception ex)
        {
            return new()
            {
                Success = false,
                Message = ex.Message
            };
        }

        return new()
        {
            Success = true,
            Sent = DateTime.Now
        };
    }
}

// Concrete managers can just implement this interface
public interface ICommsManager
{
    void SendStatement(Statement statement, Person person);
}
`
      }
    ]
  },
  {
    title: 'Liskov Substitution',
    code: [
      {
        title: 'Poor Code',
        code: `public abstract class CommsManager
{
    public abstract void SendStatement(Statement statement, Person person);

    // this will only function for SOME of the child classes
    public abstract void SendStatementWithCcs(Statement statement, Person person, IEnumerable<Person> ccs);
}

public class EmailManager : CommsManager
{
    public override void SendStatement(Statement statement, Person person)
    {
        _emailClient.Send(statement, person.emailAddress);
    }
    public override void SendStatementWithCcs(Statement statement, Person person, IEnumerable<Person> ccs)
    {
        _emailClient.Send(statement, person.emailAddress, ccs.Select(x => x.emailAddress).ToList());
    }
}

public class SmsManager : CommsManager
{
    public override void SendStatement(Statement statement, Person person)
    {
        _smsClient.Send(statement, person.phoneNumner);
    }
    public override void SendStatementWithCcs(Statement statement, Person person, IEnumerable<Person> ccs)
    {
        // SmsManager unable to send Ccs and so does not behave
        // like its parent CommsManager class violating LSP
        throw new NotImplementedException();
    }
}`
      },
      {
        title: 'Good Code',
        code: `// Two interfaces created
public interface IStatementSendable
{
    void SendStatement(Statement statement, Person person);
}
public interface IStatementCopySendable
{
    void SendStatementWithCcs(Statement statement, Person person, IEnumerable<Person> ccs);
}

public class EmailManager : IStatementSendable, IStatementCopySendable
{
    public void SendStatement(Statement statement, Person person)
    {
        _emailClient.Send(statement, person.phoneNumner);
    }
    public void SendStatementWithCcs(Statement statement, Person person, IEnumerable<Person> ccs)
    {
        _emailClient.Send(statement, person.emailAddress, ccs.Select(x => x.emailAddress).ToList());
    }
}
public class SmsManager : IStatementSendable
{
    public void SendStatement(Statement statement, Person person)
    {
        _smsClient.Send(statement, person.phoneNumner);
    }
    // SmsManager no longer needs to implement send with Ccs
    // as it only implements IStatementSendable
}`
      }
    ]
  },
  {
    title: 'Interface Segregation',
    code: [
      {
        title: 'Poor Code',
        code: `public interface IDocumentCreatable
{
    Stream CreatePdf();
    Stream CreateDocx();
    string CreateHtml();
}

public class StatementBuilder : IDocumentCreatable
{
    public Stream CreatePdf()
        => _pdfProvider.Create(_statement);
    public Stream CreateDocx()
        => _wordProvider.Create(_statement);
    public string CreateHtml()
        => _emailProvider.Create(_statement);
}

public class EmailBuilder : IDocumentCreatable
{
    // EmailBuilder should not be forced to
    // implements functionality it cannot use
    public Stream CreatePdf()
        => throw new NotImplementedException();
    public Stream CreateDocx()
        => throw new NotImplementedException();
    public string CreateHtml()
        => _emailProvider.Create(_email);
}`
      },
      {
        title: 'Good Code',
        code: `// Interface segregated into two
public interface IDocumentCreatable
{
    Stream CreatePdf();
    Stream CreateDocx();
}
public interface IHtmlCreatable
{
    string CreateHtml();
}

// StatementBuilder now implements both
public class StatementBuilder : IDocumentCreatable, IHtmlCreatable
{
    public Stream CreatePdf()
        => _pdfProvider.Create(_statement);
    public Stream CreateDocx()
        => _wordProvider.Create(_statement);
    public string CreateHtml()
        => _emailProvider.Create(_email);
}

//EmailBuilder just implements one
public class EmailBuilder : IEmailCreatable
{
    public string CreateHtml()
        => _emailProvider.Create(_email);
}
`
      }
    ]
  },
  {
    title: 'Dependency Inversion',
    code: [
      {
        title: 'Poor Code',
        code: `public class StatementService
{
    private readonly StatementBuilder _statementBuilder;
    private readonly StatementGenerator _statementGenerator;
    private readonly TransactionRepository _transationRepository;

    // constructor news up its concrete dependencies
    public StatementService()
    {
        _statementBuilder = new StatementBuilder();
        _statementGenerator = new StatementGenerator();
        _transactionRepository = new TransactionRepository();
    }

    // High level StatementService depends upon low level concrete classes
    public Stream Build(Person person, DateTime from, DateTime to)
    {
        var transactions = _transationRepository.GetTransactionsForPerson(person.Id, from, to);
        var statement = _statementGenerator.CreateStatement(person.Name, transactions);
        return _statementBuilder.CreatePdf();
    }
}`
      },
      {
        title: 'Good Code',
        code: `public class StatementService
{
    // private fields now interfaces
    private readonly IStatementBuilder _statementBuilder;
    private readonly IStatementGenerator _statementGenerator;
    private readonly ITransactionRepository _transationRepository;

    // requirements passed into service via dependency injection
    public StatementService(
        IStatementBuilder statementBuilder,
        IStatementGenerator statementGenerator,
        ITransactionRepository transactionRepository)
    {
        _statementBuilder = statementBuilder;
        _statementGenerator = statementGenerator;
        _transactionRepository = transactionRepository;
    }

    // code unchanged but now depends on abstractions
    public Stream Build(Person person, DateTime from, DateTime to)
    {
        var transactions = _transationRepository.GetTransactionsForPerson(person.Id, from, to);
        var statement = _statementGenerator.CreateStatement(person.Name, transactions);
        return _statementBuilder.CreatePdf();
    }
}`
      }
    ]
  },
];

const patterns = [
  {
    title: 'Factory',
    code: [
      {
        title: 'A typical factory pattern',
        code: ``
      }
    ]
  },
  {
    title: 'Singleton',
    code: [
      {
        title: 'A typical singleton pattern',
        code: ``
      }
    ]
  },
  {
    title: 'Proxy',
    code: [
      {
        title: 'A typical proxy pattern',
        code: ``
      }
    ]
  }
]

const firstProfile=[
  {
    title: '.NET 6',
    subTexts:[
      'Containerized microservices',
      'Unit & integration testing',
    ],
    icon: <UpgradeIcon fontSize='large'/>
  },
  {
    title: '4 years',
    subTexts:[
      'Jan \'21 - present: Account Technologies (FinTech)',
      'Jul \'20 - Dec \'20: Logical Medical Systems (Antenatal screening)',
      'Feb \'19 - Jun \'20: Anexsys (eDiscovery)',
      'Nov \'18 - Jan \'19: Bluerunner Solutions (Hospitality)'
    ],
    icon: <CalendarMonthIcon fontSize='large'/>
  },
  {
    title: 'C# 11',
    subTexts:[
      'Enjoy improving readability with new features',
      'SOLID principles',
      'Design patterns'
    ],
    icon: <DataObjectIcon fontSize='large'/>
  }
]

const secondProfile=[
  {
    title: 'Microservices',
    subTexts:[
      'Containerizable code with emphasis on simplicity',
      'HTTP, gRPC and Kafka communication'
    ],
    icon: <WorkspacesIcon fontSize='large'/>
  },
  {
    title: 'Testing',
    subTexts: [
      'Introduced integration testing framework using Specflow (Gherkin)',
      'Services written against interfaces for unit testing with Moq'
    ],
    icon: <ScienceIcon fontSize='large'/>
  }
]

export default function Dotnet() {
  return (
    <SkillTemplate theme={theme} skill='.NET'>
      <DotnetProfiles data={firstProfile} />
      <DotnetCrafts data={solid} />
      <DotnetProfiles data={secondProfile} />
      <DotnetCrafts data={patterns} />
      <DotnetProfiles data={secondProfile} />
    </SkillTemplate>
  );
}