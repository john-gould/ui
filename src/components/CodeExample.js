import { Typography } from '@mui/material';
import * as React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';

export default function CodeExample(props) {
  return (
    <div style={{maxHeight: '100vh', overflow: 'auto'}}>
      {props.code.map(x => <div>
        <Typography variant='h4'>{x.title}</Typography>
        <SyntaxHighlighter language="csharp" style={docco}>
          {x.code}
        </SyntaxHighlighter>
      </div>)}
    </div>
  );
}