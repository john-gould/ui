import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Backdrop from '@mui/material/Backdrop';
import CodeExample from './CodeExample';

export default function DotnetCraft(props) {
  const width = 225;
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };
  return (
    <div>
      <Card sx={{ width: {width} }}>
        <CardContent>
          <Typography variant='h5' color="text.secondary" gutterBottom>
            {props.title}
          </Typography>
        </CardContent>
        <CardActions>
          <Button onClick={handleToggle} size="small">Example</Button>
        </CardActions>
      </Card>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        onClick={handleClose}>
        <CodeExample code={props.code}/>
      </Backdrop>
    </div>
  );
}