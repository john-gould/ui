import * as React from 'react';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import DotnetCraft from './DotnetCraft';

export default function DotnetCrafts(props) {
  const theme = useTheme();
  const top = {
    background:`linear-gradient(to bottom right,transparent 48%,${theme.palette.secondary.main} 50%)`,
    height:40
  }
  const bottom = {
    background:`linear-gradient(to top right,transparent 48%,${theme.palette.secondary.main} 50%)`,
    height:40
  }
  return (
    <div>
      <div style={top}></div>
      <Box color='primary' sx={{
        display: 'flex',
        justifyContent: 'space-around',
        background: theme.palette.secondary.main,
        padding: '20px'
      }}>
        {props.data.map(item => <DotnetCraft
          title={item.title}
          code={item.code}
          />)}
      </Box>
      <div style={bottom}></div>
    </div>
  );
}