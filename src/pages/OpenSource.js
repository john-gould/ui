import * as React from 'react';
import Navbar from '../components/Navbar';

export default function OpenSource() {
    return (
        <Navbar skill='Open Source'/>
    );
}