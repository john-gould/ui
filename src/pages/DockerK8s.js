import * as React from 'react';
import Navbar from '../components/Navbar';

export default function DockerK8s() {
    return (
        <Navbar skill='Docker + k8s'/>
    );
}