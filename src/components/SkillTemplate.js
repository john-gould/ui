import * as React from 'react';
import { ThemeProvider } from '@mui/material';
import Footer from './Footer';
import Navbar from './Navbar';
import background from "../images/dotnetBackground.png";

const style = {
  background: `linear-gradient(0deg, rgba(255 255 255 / 97%), rgba(255 255 255 / 97%)), url(${background})`,
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  minHeight: 'calc(100vh - 320px)',
  padding: '96px 0 32px 0'
}

export default function SkillTemplate(props) {
  return (
    <ThemeProvider theme={props.theme}>
      <Navbar skill={props.skill}/>
        <div style={style}>
          {props.children}
        </div>
      <Footer/>
    </ThemeProvider>
  );
}