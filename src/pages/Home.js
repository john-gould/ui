import SkillArea from "../components/SkillArea";
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#28464B',
    },
    secondary: {
      main: '#A4B494',
    }
  }
});

function Home() {
  return (
    <ThemeProvider theme={theme}>
      <SkillArea />
    </ThemeProvider>
  );
}

export default Home;