import * as React from 'react';
import { useState } from 'react';
import Link from '@mui/material/Link';

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

const cardStyle = {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: "50vh",
    cursor: 'pointer'
}

export default function SkillCard(props) {
    const [isShown, setIsShown] = useState('hidden');
    function mouseEnter() {
        setIsShown('visible');
    }
    function mouseLeave() {
        setIsShown('hidden');
    }
    return (
        <Link underline='none' href={props.link}>
            <Box onMouseEnter={mouseEnter}
                onMouseLeave={mouseLeave}
                sx={ {background: `linear-gradient(0deg, rgba(0 0 0 / 90%), rgba(0 0 0 / 50%)), url(${props.background})`, ...cardStyle} }>
                <Typography variant='h2' style={{visibility: 'hidden'}}>
                    H
                </Typography>
                <img src={props.logo} alt={props.skill} width="100" height="100" />
                <Typography variant='h2' color='primary' style={{fontWeight: 'bold', visibility: isShown}}>
                    {props.skill}
                </Typography>
            </Box>
        </Link>
    );
}