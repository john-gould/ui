import * as React from 'react';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import gitlab from "../images/footer/gitlab.png";
import linkedin from "../images/footer/linkedin.png";
import Tooltip from '@mui/material/Tooltip';
import { Link } from '@mui/material';

export default function Footer() {
  return (
    <AppBar position="static" sx={{
      height: '192px',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      padding: '12px'
    }}>
      <Box sx={{
        height: '100%',
        width: '50vw',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
      }}>
        <Box sx={{
          display: 'flex',
          justifyContent: 'space-around',
          alignItems: 'center'
        }}>
          <Tooltip title='Gitlab'>
            <Link href='https://gitlab.com/johngould9487'>
              <img src={gitlab} alt='gitlab' height="50" />
            </Link>
          </Tooltip>
          {/* <Tooltip title='CV'>
            <Link href='/'>
              <Box sx={{
                  display: 'flex',
                  alignItems: 'center',
                  flexDirection: 'column'
                }}>
                <AccountCircleIcon fontSize='large' sx={{color:'black !important'}}/>
                <Typography variant='h6' color='black' align='center'>CV</Typography>
              </Box>
            </Link>
          </Tooltip> */}
          <Tooltip title='LinkedIn'>
            <Link href='https://www.linkedin.com/in/johngould9487'>
              <img src={linkedin} alt='LinkedIn' height="50" />
            </Link>
          </Tooltip>
        </Box>
      </Box>
    </AppBar>
  );
}