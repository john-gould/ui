import * as React from 'react';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import DotnetProfile from './DotnetProfile';

export default function DotnetProfiles(props) {
  const theme = useTheme();
  const divider = {
    width: '1px',
    height: '128px',
    background: theme.palette.primary.main
  }
  return (
    <Box sx={{
        padding: '20px 0',
        display: 'flex',
        justifyContent: 'space-evenly',
        alignItems: 'center'
      }}>
      {[].concat(...props.data.map(item => <DotnetProfile
        icon={item.icon}
        title={item.title}
        vw={100/props.data.length}
        subTexts={item.subTexts} />)
        .map(n => [n, <div style={divider}></div>])).slice(0,-1)}
    </Box>
  );
}