const a = {
  type: 'text',
  content: ` and `
};
const w = {
  type: 'text',
  content: ` with `
};

const blog = [
  {
    type: 'text',
    content: 'The equation in question is'
  },
  {
    type: 'equation',
    content: String.raw`\ddot{x} + 2\beta\dot{x} + \omega_0^2x = 0\,.`
  },
  {
    type: 'text',
    content: `It describes scenarios where the restoring force is proportional to negative the
    displacement and there is a resistive force proportional to the velocity. Think of a mass on a
    spring subjected to air resistance. The initial conditions will be as generic as possible:
    `
  },
  {
    type: 'inline',
    content: String.raw`x(0)=X_0`
  },
  a,
  {
    type: 'inline',
    content: String.raw`\dot{x}(0)=v`
  },
  w,
  {
    type: 'inline',
    content: String.raw`X_0`
  },
  a,
  {
    type: 'inline',
    content: String.raw`v`
  },
  {
    type: 'text',
    content: ` both real (obvious but important for later).`
  },
  {
    type: 'break',
  },
]

export default blog;